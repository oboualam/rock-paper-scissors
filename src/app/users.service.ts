import { Injectable } from '@angular/core';
import { Player } from './models/Player.model';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  players: any = [];
  constructor() { }


  createUser(user): void{
    this.players.push(user);
  }

  deleteUser(user): void{
    var tamp : any = [];
    for(let i = 0;  i < this.players.length; i++){
      if(this.players[i].name != user.name){
        tamp.push(this.players[i]);
      }
    }
    this.players = tamp;
    this.createUser(user);
  }


  getAllPlayers() : any [] {
     return this.players.sort(this.sortFunction);
  }

  getPlayer(name : String) : any {
    var p = this.players.find(p => p.name == name);
    return p;
  }


 sortFunction(a, b) {
    if (a.por === b.por) {
        return 0;
    }
    else {
        return (a.por > b.por) ? -1 : 1;
    }
}
}
