import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../users.service';
import $ from "jquery";
 
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  name: String  = '';
  enemySelected  = -1;
  scores = [0 , 0];

  playerSelected = -1;
  loading= false;
  theResult = 0 
  usersService: UsersService;

  constructor(private router: Router,  private route: ActivatedRoute,usersService: UsersService) {
    this.usersService = usersService;
   }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('name');
  }


  select( arm: number): void {
   if(this.loading) return;
   this.loading = true;
   this.playerSelected = arm;
 
   setTimeout( () => {
     this.loading = false;
     const randomNum =  Math.floor(Math.random() * 3 ) ;
     this.enemySelected = randomNum;
     this.checkResult();
   },  Math.floor(Math.random()  * 500 ) +200);
 }


 checkResult(): void {
  const playerSelect = this.playerSelected;
  const enemySelect = this.enemySelected;
  if( playerSelect ==  enemySelect)
   {
   this.theResult = 2;
 }
 
   else if ( (playerSelect - enemySelect + 3)% 3 == 1)    {
     this.theResult = 0;
     this.scores[0] = this.scores[0]+1;
     $(".player").addClass("me");
     setTimeout(() => {
      $(".player").removeClass("me");
     }, 2000);
   }
   else{
     this.theResult = 1;
       this.scores[1] = this.scores[1]+1;
       $(".machine").addClass("me");
       setTimeout(() => {
        $(".machine").removeClass("me");
       }, 2000);
   }
}

exite(){
  
  var user = this.usersService.getPlayer(this.name);
   user.win += this.scores[0];
  user.los += this.scores[1];
  user.por = (user.win / (user.win + user.los) * 100);
  this.usersService.deleteUser(user);
  //this.usersService.createUser(user);
  this.router.navigate(['/'])
 }

}
