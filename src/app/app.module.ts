import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { GameComponent } from './game/game.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
 
@NgModule({
  declarations: [
    AppComponent,
    CreateUserComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFontAwesomeModule,
   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
