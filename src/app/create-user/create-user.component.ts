import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
import { Player } from '../models/Player.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  name: string = '';
  model: any = {};
  hideElement: boolean = false;
  player: Player[];
  usersService: UsersService;
  getPlayers : any [];
  isNaN: Function = Number.isNaN;
  
  constructor(private router: Router, usersService: UsersService) { 
    this.usersService = usersService;
  }

  ngOnInit() {
    this.getPlayers = this.usersService.getAllPlayers();
  }

  createUser(){
    if(this.model.name != undefined){
      const player = new Player(
          this.model.name,
          0,
          0,
          0
      )
     this.usersService.createUser(player);
     this.router.navigate(['/game/' + this.model.name])
    }else{
      this.hideElement = true;
    }
    
  }
}
